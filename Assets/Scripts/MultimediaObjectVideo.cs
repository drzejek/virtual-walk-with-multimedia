﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultimediaObjectVideo : MultimediaObject
{
    private GameObject _camera;
    private RectTransform _rectTrans;
    private Mesh _planeMesh;
    private UnityEngine.Video.VideoPlayer _videoPlayer;

    private float timer;
    private float timeToWait = 1;
    private int counter;

    // Start is called before the first frame update
    void Start()
    {
        _rend = GetComponent<Renderer>();
        _planeMesh = GetComponent<MeshFilter>().mesh;


        minScale = transform.localScale;
        counter = 0;
        timer = 0;
        SetColor();

        _videoPlayer = GetComponent<UnityEngine.Video.VideoPlayer>();
        _videoPlayer.playOnAwake = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void PullIn()
    {
        transform.localPosition = positionOut;
        StartCoroutine(AnimateIn());
    }
    public override void PushOut()
    {
        isPositionOut = true;
        StartCoroutine(AnimateOut());
    }
    public override void PlayMultimedia(){
        _videoPlayer.Play();
    }
    public override void StopMultimedia()
    {
        _videoPlayer.Stop();
    }
    public override Vector3 GetTopRightCorner(Vector3 position)
    {
        float xLocalScale = (maxScale.x * 10) / 2;
        float zLocalScale = (maxScale.z * 10) / 2;
        Vector3 topRightCorner = new Vector3(
            position.x - 0.01f,
            position.y + zLocalScale,
            position.z - xLocalScale);
        return topRightCorner;
    }
}
