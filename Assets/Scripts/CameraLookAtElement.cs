﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAtElement : MonoBehaviour
{
    private Camera _cam;
    private GameObject _gameObject;
    private RaycastReceiver _raycastReceiver;
    private RaycastReceiver _prevRaycastReceiver;

    public ClosePoint _closePnt;
    public ClosePoint _prevClosePnt;

    private int counterBegin;
    private int counterLoaderBegin;
    private int counterEnd;
    private int counterLoaderEnd;
    private float timerBegin;
    private float timerEnd;
    private float timeToWait = 3;


    void Start() {
        _cam = GetComponent<Camera>();
        counterBegin = 0;
        counterEnd = 0;
        counterLoaderBegin = 0;
        counterLoaderEnd = 0;
    }

    void Update()
    {

        Vector3 point = new Vector3(_cam.pixelWidth / 2, _cam.pixelHeight / 2, 0);
        Ray ray = _cam.ScreenPointToRay(point);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            _raycastReceiver = hit.collider.GetComponent<RaycastReceiver>();
            _closePnt = hit.collider.GetComponent<ClosePoint>();
            if (_raycastReceiver != null)
            {
                counterLoaderBegin++;
                _prevRaycastReceiver = _raycastReceiver;
                _raycastReceiver.RaycastCameraPointInChangeColor();
                if (counterLoaderBegin == 1){
                    _raycastReceiver.StartLoadingAnimationOnBegin();
                }

                timerBegin += Time.deltaTime;
                if (timerBegin > timeToWait && counterBegin == 0)
                {
                    counterBegin++;
                    if (_raycastReceiver != null)
                    {
                        _raycastReceiver.AnimateIn();
                    }
                    timerBegin = 0;
                }
            }

            if (_closePnt != null)
            {
                counterLoaderEnd++;
                _prevClosePnt = _closePnt;
                if (counterLoaderEnd == 1){
                    _closePnt.StartLoadingAnimationOnEnd();
                }

                timerEnd += Time.deltaTime;
                if (timerEnd > timeToWait && counterEnd == 0)
                {
                    counterEnd++;
                    if (_prevRaycastReceiver != null)
                    {
                        _prevRaycastReceiver.AnimateOut();
                    }
                    timerEnd = 0;
                }
            }
        }
        else
        {
            if (_prevRaycastReceiver != null)
            {
                _prevRaycastReceiver.RaycastCameraPointOutChangeColor();
                _prevRaycastReceiver.StopLoadingAnimation();

                //_prevRaycastReceiver._loadingBegin._video.frame = 0;
                Debug.Log(_prevRaycastReceiver._loadingBegin._video.frame);

                timerBegin = 0;
                timerEnd = 0;
                counterBegin = 0;
                counterEnd = 0;
                counterLoaderBegin = 0;
                counterLoaderEnd = 0;
            }

            if(_prevClosePnt != null){
                _prevClosePnt.StopLoadingAnimation();
            }
        }
    }
}
