﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBarBegin : MonoBehaviour
{
    public UnityEngine.Video.VideoPlayer _video;
    // Start is called before the first frame update
    void Start()
    {
        _video = GetComponentInChildren<UnityEngine.Video.VideoPlayer>();
        _video.enabled = false;
        _video.frame = 0;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayVideo()
    {
        _video.enabled = true;
    }

    public void StopVideo()
    {
        //_video.enabled = false;
        _video.frame = 0;
    }
}
