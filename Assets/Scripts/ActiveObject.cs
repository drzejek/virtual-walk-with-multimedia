﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveObject : MonoBehaviour
{
    Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = gameObject.GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit) && hit.transform.name == gameObject.name)
        {
            print("I'm looking at " + hit.transform.name);
            gameObject.SetActive(false);
        }
        else
        {
            print("I'm looking at nothing!");
            gameObject.SetActive(false);
        }
    }
}
