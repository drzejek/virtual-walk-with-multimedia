﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pointer : MonoBehaviour
{
    public int index;
    protected Renderer _rend;
    public Vector3 positionIn;
    public Vector3 positionOut;

    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {

    }
    public virtual void CameraPointInChangeColor()
    {

    }

    public virtual void CameraPointOutChangeColor()
    {
        
    }
    public virtual void PushOut()
    {
        
    }

    public virtual void PullIn()
    {

    }
}
