﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultimediaObject : MonoBehaviour
{
    protected Renderer _rend;
    protected Vector3 minScale;
    private Vector4 inColor;

    public Vector3 maxScale;
    private Vector4 outColor;
    protected bool isPositionOut;

    protected float localPositionX;
    public float speed;
    public float duration;
    protected float i;

    public Vector3 positionOut;
    public Vector3 positionIn;

    // Start is called before the first frame update
    void Start()
    {
        _rend = GetComponent<Renderer>();
        positionIn = transform.position;
        isPositionOut = false;

        minScale = transform.localScale;
        SetColor();
    }

    public void SetColor()
    {
        outColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);
        inColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
        _rend.material.color = outColor;
    }
    public IEnumerator RepeatLerp(Vector3 a, Vector3 b, Vector4 c, Vector4 d, float time)
    {
        i = 0.0f;
        float rate = (1.0f / time) * speed;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(a, b, i);
            _rend.material.color = Vector4.Lerp(c, d, i);

            if(i >= 1.0f && isPositionOut) {
                transform.localPosition = positionIn;
                isPositionOut = false;
            }
            yield return null;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator AnimateIn()
    {
        yield return RepeatLerp(minScale, maxScale, inColor, outColor, duration);
    }

    public IEnumerator AnimateOut()
    {
        yield return RepeatLerp(maxScale, minScale, outColor, inColor, duration);
    }
    public virtual void PullIn(){
    }

    public virtual void PushOut(){
    }

    public virtual void PlayMultimedia() {
    }

    public virtual void StopMultimedia(){
    }

    public Vector3 GetGlobalPosition()
    {
        return transform.position;
    }

    public virtual Vector3 GetTopRightCorner(Vector3 position)
    {
        return position;
    }
    public virtual Vector2 GetRightCorner(){
        return new Vector2(0, 0);
    }
}
