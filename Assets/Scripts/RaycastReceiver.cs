﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastReceiver : MonoBehaviour
{
    public MultimediaObject _multObj;
    public Pointer _pointer;
    private Renderer _rend;
    public LoadingBarBegin _loadingBegin;
    private ClosePoint _closePoint;
    public Camera _camera;
    private List<Vector3> cameraPositionList;

    // Start is called before the first frame update
    void Start()
    {
        _rend = GetComponent<Renderer>();
        _multObj = GetComponentInChildren<MultimediaObject>();
        _pointer = GetComponentInChildren<Pointer>();
        _loadingBegin = GetComponentInChildren<LoadingBarBegin>();
        _closePoint = GetComponentInChildren<ClosePoint>();
        _camera = Camera.main;

        cameraPositionList = new List<Vector3>();
        cameraPositionList.Add(new Vector3(0f, 0f, 0f));
        cameraPositionList.Add(new Vector3(-1.749f, 0f, -2.543f));
        cameraPositionList.Add(new Vector3(-3.753f, 0f, -0.201f));
    }

    // Update is called once per frame
    void Update()
    {
        if(_multObj != null)
        {
            _closePoint.transform.position = _multObj.GetGlobalPosition();
            _closePoint.transform.localPosition = _multObj.GetTopRightCorner(_closePoint.transform.localPosition);

        }
    }

    public void RaycastCameraPointInChangeColor()
    {
        _pointer.CameraPointInChangeColor();
    }
    public void RaycastCameraPointOutChangeColor()
    {
        _pointer.CameraPointOutChangeColor();
    }

    public void StartLoadingAnimationOnBegin()
    {
        _loadingBegin.PlayVideo();
    }

    public void StopLoadingAnimation()
    {
        _loadingBegin.StopVideo();
    }

    public void AnimateIn() {
        if(_pointer.GetType() == typeof(PointerToMultimedia))
        {
            _multObj.PullIn();
            _multObj.PlayMultimedia();
            _pointer.PushOut();
        }
        else
        {
            _camera.transform.position = cameraPositionList[_pointer.index]; ;
        }
    }

    public void AnimateOut()
    {
        _multObj.StopMultimedia();
        _multObj.PushOut();
        _pointer.PullIn();
    }
}
