﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosePoint : MonoBehaviour
{
    private LoadingBarEnd _loadingEnd;
    public MultimediaObject _multObj;

    // Start is called before the first frame update
    void Start()
    {
        _loadingEnd = GetComponentInChildren<LoadingBarEnd>();

    }

    // Update is called once per frame
    void Update()
    {
    }

    public void StartLoadingAnimationOnEnd()
    {
        _loadingEnd.PlayVideo();
    }

    public void StopLoadingAnimation()
    {
        _loadingEnd.StopVideo();
    }
}
