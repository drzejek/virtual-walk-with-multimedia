﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerToMultimedia : Pointer
{
    void Start()
    {
        positionIn = transform.localPosition;
        _rend = GetComponent<Renderer>();
        _rend.material.color = new Color(0, 0, 1, 0.5f);
    }

    public override void CameraPointInChangeColor()
    {
        _rend.material.color = new Color(0, 1, 0, 0.5f);
    }

    public override void CameraPointOutChangeColor()
    {
        _rend.material.color = new Color(0, 0, 1, 0.5f);
    }
    public override void PushOut()
    {
        transform.localPosition = positionOut;
    }

    public override void PullIn()
    {
        transform.localPosition = positionIn;
    }
}
