﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBarEnd : MonoBehaviour
{
    private UnityEngine.Video.VideoPlayer _video;
    // Start is called before the first frame update
    void Start()
    {
        _video = GetComponentInChildren<UnityEngine.Video.VideoPlayer>();
        _video.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayVideo()
    {
        _video.enabled = false;
        _video.enabled = true;
    }

    public void StopVideo()
    {
        _video.enabled = false;
    }
}
